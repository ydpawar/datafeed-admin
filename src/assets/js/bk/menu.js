!function (n) {
  "use strict";
  var t = function () {
  };

  t.prototype.init = function () {
    this.initTooltipPlugin(), this.initPopoverPlugin(), this.initToastPlugin(), this.initSlimScrollPlugin(), this.initFormValidation(), this.initCustomModalPlugin(), this.initCounterUp(), this.initPeityCharts(), this.initKnob(), this.initTippyTooltips()
  }, n.Components = new t, n.Components.Constructor = t
}(window.jQuery),
  function (a) {
    "use strict";
    var t = function () {
      this.$body = a("body"), this.$portletIdentifier = ".card", this.$portletCloser = '.card a[data-toggle="remove"]', this.$portletRefresher = '.card a[data-toggle="reload"]'
    };
    t.prototype.init = function () {
      var o = this;
      a(document).on("click", this.$portletCloser, function (t) {
        t.preventDefault();
        var i = a(this).closest(o.$portletIdentifier),
          e = i.parent();
        i.remove(), 0 == e.children().length && e.remove()
      }), a(document).on("click", this.$portletRefresher, function (t) {
        t.preventDefault();
        var i = a(this).closest(o.$portletIdentifier);
        i.append('<div class="card-disabled"><div class="card-portlets-loader"></div></div>');
        var e = i.find(".card-disabled");
        setTimeout(function () {
          e.fadeOut("fast", function () {
            e.remove()
          })
        }, 500 + 5 * Math.random() * 300)
      })
    }, a.Portlet = new t, a.Portlet.Constructor = t
  }(window.jQuery),
  function (e) {
    "use strict";
    var t = function () {
      this.$body = e("body"), this.$window = e(window)
    };
    t.prototype._resetSidebarScroll = function () {
      e(".slimscroll-menu").slimscroll({
        height: "auto",
        position: "right",
        size: "8px",
        color: "#9ea5ab",
        wheelStep: 5,
        touchScrollStep: 20
      })
    },

      t.prototype.initMenu = function () {
        var i = this;
        e(".mobile-button").on("click", function (t) {
          t.preventDefault(), i.$body.toggleClass("sidebar-enable"), 768 <= i.$window.width() ? i.$body.toggleClass("enlarged") : i.$body.removeClass("enlarged"), i._resetSidebarScroll()
        }), e("#side-menu").metisMenu(), i._resetSidebarScroll(), e(".right-side").on("click", function (t) {
          e("body").toggleClass("right-bar-enabled")
        }), e(document).on("click", "body", function (t) {
          0 < e(t.target).closest(".right-side, .right-bar").length || 0 < e(t.target).closest(".left-side-menu, .side-nav").length || e(t.target).hasClass("mobile-button") || 0 < e(t.target).closest(".mobile-button").length || (e("body").removeClass("right-bar-enabled"), e("body").removeClass("sidebar-enable"))
        }), e("#side-menu a").each(function () {
          var t = window.location.href.split(/[?#]/)[0];
          this.href == t && (e(this).addClass("active"), e(this).parent().addClass("active"), e(this).parent().parent().addClass("in"), e(this).parent().parent().prev().addClass("active"), e(this).parent().parent().parent().addClass("active"), e(this).parent().parent().parent().parent().addClass("in"), e(this).parent().parent().parent().parent().parent().addClass("active"))
        }), e(".navbar-toggle").on("click", function (t) {
          e(this).toggleClass("open"), e("#navigation").slideToggle(400)
        }), e(window).on("load", function () {
          e("#status").fadeOut(), e("#preloader").delay(350).fadeOut("slow")
        })
      },

      t.prototype.initLayout = function () {
        768 <= this.$window.width() && this.$window.width() <= 1028 ? this.$body.addClass("enlarged") : 1 != this.$body.data("keep-enlarged") && this.$body.removeClass("enlarged")
      },

      t.prototype.init = function () {
        var i = this;
        this.initLayout(), e.Portlet.init(), this.initMenu(), e.Components.init(), i.$window.on("resize", function (t) {
          t.preventDefault(), i.initLayout(), i._resetSidebarScroll()
        })
      },

      e.App = new t, e.App.Constructor = t
  }(window.jQuery),
  function (t) {
    "use strict";
    window.jQuery.App.init()
  }(), Waves.init();
