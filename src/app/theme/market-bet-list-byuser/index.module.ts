import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MarketBetListByUserComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: MarketBetListByUserComponent,
  },
  {
    path: ':uid/:mtype/:eid',
    component: MarketBetListByUserComponent,
  },
  {
    path: ':uid/:mtype/:eid/:mid',
    component: MarketBetListByUserComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    MarketBetListByUserComponent
  ]
})
export class MarketBetListByUserModule {
}
