import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RulesService } from '../../../_api/index';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';

interface Rules {
  id: string;
  sport: string;
  market: string;
  rules: string;
  status: {
    val: string; text: string; cssClass: string;
  };
}

class RulesObj implements Rules {
  id: string;
  sport: string;
  market: string;
  rules: string;
  status: {
    val: string; text: string; cssClass: string;
  };
}

@Component({
  selector: 'app-manage',
  templateUrl: './index.component.html',
  providers: [RulesService]
})

export class ManageComponent implements OnInit, AfterViewInit {

  title = 'Rules';
  createUrl = '/rules/create';
  breadcrumb: any = [{title: 'Rules', url: '/' }, {title: 'Manage', url: '/' }];

  page = { start: 1, end: 5 };

  aList: Rules[] = [];
  cItem: Rules;
  isEmpty = false;
  isLoading = false;

  constructor( private service: RulesService, private loadJs: ScriptLoaderService ) { }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    this.service.manage().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data;
        const data: Rules[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: Rules = new RulesObj();

            cData.id = item.id;
            cData.sport = item.sport;
            cData.market = item.market;
            cData.rules = item.content;
            cData.status = {
              val: item.status,
              text: item.status === 1 ? 'Active' : 'Inactive',
              cssClass: item.status === 1 ? 'border-success text-success' : 'border-danger text-danger'
            };

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.loadJs.load('script' , 'assets/js/datatables.init.js');

      }
    }
  }


  changeStatus(item) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        // tslint:disable-next-line:triple-equals
        const data = { id: item.id, status: ( item.status.val == 1 ? 0 : 1 )};
        this.service.status(data).subscribe((res) => this.onChangeStatusSuccess(res));
      }
    });
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

}
