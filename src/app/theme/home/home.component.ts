import { Component, OnInit , OnDestroy} from '@angular/core';
import { DashboardService} from '../../_api';
import {AuthIdentityService, ToastrService} from '../../_services';
import swal from "sweetalert2";
import {NgxSpinnerService} from 'ngx-spinner';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

declare var $;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DashboardService]
})
export class HomeComponent implements OnInit, OnDestroy{

  dataList: any = [];
  eventList: any = [];
  marketList: any = [];
  marketIdsArr: any = [];
  isActiveTabs: any = [];
  isEmpty = true;
  cUserData: any;
  private isDestroy: boolean = false;

  type:string = 'all';
  allActive:number;

  title = 'Dashboard';
  breadcrumb: any = [];
  isLoad: boolean = false;
  // breadcrumb: any = [{title: 'Manage', url: '/' }];

  // tslint:disable-next-line:max-line-length
  constructor(
      private service: DashboardService,
      private authIdentity: AuthIdentityService,
      private spinner: NgxSpinnerService,
      private toaster: ToastrService) {

      // this.isActiveTabs = 'all';

      if (authIdentity.isLoggedIn()) {
        this.cUserData = authIdentity.getIdentity();
      }
  }

  ngOnInit() {
    // this.getDataList();
    // if ( window.localStorage.getItem('loadBanner') && window.localStorage.getItem('loadBanner') === 'yes' ) {
    //   $('.modal-s3').modal('show');
    //   $('body').removeClass('modal-open');
    //   window.localStorage.removeItem('loadBanner');
    // }
  }
  
  ngOnDestroy() {
      this.isDestroy = true;
  }


  async getDataList() {
    if(!this.isLoad) {
      this.spinner.show();
    }
     await this.service.getMarket().subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        (data) => {
          //console.log(data);
          this.onSuccessDataList(data);
        },
        error => {
          this.toaster.error(error.message, 'Something want wrong..!');
        });
  }

  onSuccessDataList(response) {
    this.isLoad = true;
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.isEmpty = true;
        this.eventList = this.marketList = this.dataList = [];

        // if ( response.data.length > 0 ) {
        //   this.dataList = response.data;
        // }

        // if ( response.userData ) {
        //   this.cUserData = response.userData;
        // }

        // if ( response.event && response.event.length > 0 ) {
        //     this.isEmpty = false;
        //     this.eventList = response.event;
        // }

        if (response.data && response.data.list && response.data.list.length > 0 ) {
          this.isEmpty = false;
          this.marketList = response.data.list;
          this.allActive = response.data.is_block_all;
        }
        
        if (!this.isDestroy) {
          const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getDataList(); }, 10000);
        }
        // if (response.data[3]){
        //   this.allActive = response.data[3].is_block_all;
        //   // console.log(this.allActive);
        // }
      }
    } 
    this.spinner.hide();
  }

  changeStatus(tId,status) {
    let statustxt = status === 0 ? 'In-active' : 'active';
    swal.fire({
      title: 'Are you sure to want ' + statustxt + ' ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        const data = {type: tId, active: status === 1 ? 0 : 1 };
        this.service.status(data).subscribe((res) => this.onChangeStatusSuccess(res));
      }
    });
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
     this.getDataList();
    }
  }
  
  doEventBlock(eid, status) {
    let statustxt = 'block';
    if ( status ) {
      statustxt = 'unblock';
    }
    swal.fire({
      title: 'Are you sure to want ' + statustxt + ' ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.doEventBlock(eid).subscribe((res) => this.onSuccessEventBlock(res));
      }
    });
  }

  onSuccessEventBlock(res) {
    if (res.status === 1) {
      this.getDataList();
    }
  }

  doSportBlock(eid, status) {
    let statustxt = 'block';
    if ( status ) {
      statustxt = 'unblock';
    }
    swal.fire({
      title: 'Are you sure to want ' + statustxt + ' ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.doSportBlock(eid).subscribe((res) => this.onSuccessSportBlock(res));
      }
    });
  }

  onSuccessSportBlock(res) {
    if (res.status === 1) {
      this.getDataList();
    }
  }

}
