import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExcelService } from 'src/app/common/excel.service';
import { EnquiryService } from 'src/app/_api/enquiry/enquiry.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'src/app/_services/toastr.service';

declare var $, swal;
@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.css'],
  providers: [EnquiryService]
})
export class EnquiryComponent implements OnInit {

  page = { start: 1, end: 5 };
  dataList: any = [];
  eventList: any = [];
  newsList: any = [];

  isEmpty = true;
  cUserData: any;

  isLoad: boolean = false;

  constructor(
    private service: EnquiryService,
    private spinner: NgxSpinnerService,
    private toaster: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.getEnquiryList();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {

  }


  getEnquiryList() {
    this.spinner.show();
    this.service.getEnquriy().subscribe(
      (data) => {
        // console.log(data);
        this.onSuccessDataList(data);
      },
      error => {
        this.toaster.error(error.message, 'Something want wrong..!');
      });
  }

  onSuccessDataList(response) {
    this.spinner.hide();

    if (response.status === 1) {
      this.isEmpty = true;
      this.dataList = response.data;
    }
  }

  doDeleteEnquiry(uid){
    swal.fire({
      title: 'Are you sure to delete ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.deleteEnquiry(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
      }
    });
  }

  onSuccessStatusUpdate(res) {
    this.toaster.success(res.message);

    if (res.status === 1) {
      this.getEnquiryList();
    }

  }
}
