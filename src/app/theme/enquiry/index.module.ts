import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {EnquiryComponent} from './enquiry.component';

const routes: Routes = [
  {
    path: '',
    component: EnquiryComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    EnquiryComponent
  ]
})
export class EnquiryModule {
}
