import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {HistoryService} from '../../../_api';
import {ScriptLoaderService} from '../../../_services';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  description: string;
  client: string;
  price: string;
  rate: string;
  stake: string;
  pl: string;
  ip: string;
  master: string;
  time: string;
  bType: string;
  result: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  description: string;
  client: string;
  price: string;
  rate: string;
  stake: string;
  pl: string;
  ip: string;
  master: string;
  time: string;
  bType: string;
  result: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class MarketBetComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Market Bet';
  breadcrumb: any = [{title: 'Market Bet', url: '/' }];

  page = { start: 1, end: 5 };

  uid = '';
  mtype: string;
  mid: string;
  aList: listData[] = [];
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_market_bet';

  constructor(
      private service: HistoryService,
      private loadJs: ScriptLoaderService,
      private route: ActivatedRoute,
      private spinner: NgxSpinnerService
  ) {
    this.mid = this.route.snapshot.params.mid;
    // tslint:disable-next-line:triple-equals
    if ( window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined ) {
      this.title = 'Market Bet List for ' + window.localStorage.getItem('title');
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
  }

  ngOnDestroy() {
    window.localStorage.removeItem('title');
    window.localStorage.removeItem(this.dataTableId);
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    const data = { mid: this.mid };
    this.service.marketBets(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        $('#DataTables_market_bet').dataTable().fnDestroy();
        const items = res.data.list;
        const data: listData[] = [];

        if ( res.data.count > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.betId;
            cData.description = item.description;
            cData.client = item.client;
            cData.price = item.price;
            cData.rate = item.rate;
            cData.stake = item.size;
            if ( item.mType === 'match_odd' || item.mType === 'bookmaker' || item.mType === 'fancy3' ) {
              cData.rate = '-';
            } else {
              cData.rate = item.rate;
            }
            cData.stake = item.size;
            if ( item.bType === 'back' || item.bType === 'yes') {
              cData.pl = item.win;
            } else {
              cData.pl = item.loss;
            }
            cData.ip = item.ip_address;
            cData.master = item.master;
            cData.time = item.created_on;
            cData.bType = item.bType;
            cData.result = item.result;

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.loadScript();
      }
    }
    this.spinner.hide();
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#DataTables_market_bet').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }
}
