import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {HistoryService} from '../../../../_api/index';
import {BaseComponent} from '../../../../common/commonComponent';

declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  name: string;
  sportId: string;
  eventId: string;
  pl: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  name: string;
  sportId: string;
  eventId: string;
  pl: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class ListComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Live Game Profit Loss';
  breadcrumb: any = [{title: 'Live Game Profit Loss', url: '/' }, {title: 'List', url: '/' }];

  page = { start: 1, end: 5 };

  uid = '';
  aList: listData[] = [];
  aListTotal: any;
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_teenpatti_pl';

  constructor(
      inj: Injector,
      private service: HistoryService,
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.teenpattiProfitLoss(this.uid).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_teenpatti_pl').dataTable().fnDestroy();
      if ( res.userName !== undefined ) {
        this.title = 'Live Game Profit Loss for ' + res.userName;
      }
      if (res.data !== undefined) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.sportId = item.sportId;
            cData.eventId = item.eventId;
            cData.pl = item.profitLoss;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }
        if (res.total) {
          this.aListTotal = res.total;
        }
        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.initDataTables('DataTables_teenpatti_pl');
      }
    }
    this.spinner.hide();
  }

}

