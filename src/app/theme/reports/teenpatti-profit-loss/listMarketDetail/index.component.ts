import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {HistoryService} from '../../../../_api/index';
import {AuthIdentityService} from '../../../../_services';
import {BaseComponent} from '../../../../common/commonComponent';
import {Validators} from '@angular/forms';
import {ExcelService} from '../../../../common/excel.service';

declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  time: string;
  description: string;
  marketId: string;
  pl: string;
  comm: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  time: string;
  description: string;
  marketId: string;
  pl: string;
  comm: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class ListMarketDetailComponent extends  BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Live Game Profit Loss';
  breadcrumb: any = [{title: 'Live Game Profit Loss', url: '/' }, {title: 'List', url: '/' }];
  page = { start: 1, end: 5 };
  user: any;
  uid = '';
  sid: string;
  eid: string;
  aList: listData[] = [];
  aListTotal: any;
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_teenpatti_pl_market';

  constructor(
      inj: Injector,
      private service: HistoryService,
      private excelSheet: ExcelService
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;
    this.sid = this.activatedRoute.snapshot.params.sid;
    this.eid = this.activatedRoute.snapshot.params.eid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    const data = {sid: this.sid, eid: this.eid};
    this.service.profitLossMarket(this.uid, data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_teenpatti_pl_market').dataTable().fnDestroy();
      if ( res.userName !== undefined ) {
        this.title = 'Live Game Profit Loss for ' + res.userName;
      }
      if (res.data !== undefined) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.time = item.time;
            cData.description = item.description;
            cData.marketId = item.mid;
            cData.pl = item.pl;
            cData.comm = item.comm;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }
        if (res.total) {
          this.aListTotal = res.total;
        }
        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.initDataTables('DataTables_teenpatti_pl_market');

      }
    }
    this.spinner.hide();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      start: ['', Validators.required],
      end: ['', Validators.required],
      sid: [this.sid],
      eid: [this.eid],
    });
  }

  submitForm(fType = null) {
    if ( fType != null ) {
      const data = this.frm.value; data.ftype = fType;
      this.service.profitLossMarket(this.uid, data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        const data = this.frm.value;
        this.service.profitLossMarket(this.uid, data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      // this.frm.reset();
      this.onSuccess(res);
    }
  }

  excelDownload() {
    const tmpData = [];
    let tmp = {
      NO: '',
      TIME: '',
      DESCRIPTION: '',
      PROFIT_LOSS: '',
      COMM: ''
    };
    tmpData.push(tmp);
    this.aList.forEach((item, index) => {
      const pl = parseFloat(item.pl) !== 0 ? item.pl : '0.0';
      const comm = parseFloat(item.comm) !== 0 ? item.comm : '0.0';
      const i = (index + 1).toString();
      tmp = {
        NO: i,
        TIME: item.time,
        DESCRIPTION: item.description,
        PROFIT_LOSS: pl,
        COMM: comm
      };
      tmpData.push(tmp);
    });
    this.excelSheet.exportAsExcelFile(tmpData, 'profit-loss');
  }

  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

}

