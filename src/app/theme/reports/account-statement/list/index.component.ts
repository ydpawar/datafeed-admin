import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {HistoryService} from '../../../../_api/index';
import {Validators} from '@angular/forms';
import {ExcelService} from '../../../../common/excel.service';
import {BaseComponent} from '../../../../common/commonComponent';

declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  clientId:string;
  clientName:string;
  description: string;
  amount: string;
  balance: string;
  time: string;
  remark: string;
  type: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  clientId:string;
  clientName:string;
  description: string;
  amount: string;
  balance: string;
  time: string;
  remark: string;
  type: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class ListComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'Account Statement';
  breadcrumb: any = [{title: 'Account Statement', url: '/' }, {title: 'List', url: '/' }];

  page = { start: 1, end: 5 };

  uid = '';
  aList: listData[] = [];
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_account_statement';
  userName: string;

  constructor(
      inj: Injector,
      private service: HistoryService,
      private excelSheet: ExcelService
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;
  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.accountStatement(this.uid, {}).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_account_statement').dataTable().fnDestroy();
      if ( res.userName !== undefined ) {
        this.title = 'Account Statement for ' + res.userName;
      }

      if ( res.data !== undefined ) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.clientId = item.clientId;
            cData.clientName = item.clientName;
            cData.description = item.description;
            cData.amount = item.amount;
            cData.balance = item.balance;
            cData.time = item.updated_on;
            cData.remark = item.remark;
            cData.type = item.type;

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.initDataTables('DataTables_account_statement');

      }
    }
    this.spinner.hide();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      type: ['', Validators.required],
      start: ['', Validators.required],
      end: ['', Validators.required],
    });
  }

  submitForm(fType = null) {
    if ( fType != null ) {
      const data = this.frm.value; data.ftype = fType;
      this.service.accountStatement(this.uid, data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        const data = this.frm.value;
        this.service.accountStatement(this.uid, data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.onSuccess(res);
    }
  }

  excelDownload() {
    const tmpData = [];
    let tmp = {
      NO: '',
      CLIENTID:'',
      CLIENT_Name:'',
      DESCRIPTION: '',
      CREDIT: '',
      DEBIT: '',
      BALANCE: '',
      DATE_TIME: '',
      REMARK: ''
    };
    tmpData.push(tmp);
    this.aList.forEach((item, index) => {
      const credit = parseFloat(item.amount) > 0 ? item.amount : '0.00';
      const debit = parseFloat(item.amount) < 0 ? item.amount : '0.00';
      const i = (index + 1).toString();
      tmp = {
        NO: i,
        CLIENTID: item.clientId,
        CLIENT_Name: item.clientName,
        DESCRIPTION: item.description,
        CREDIT: credit,
        DEBIT: debit,
        BALANCE: item.balance,
        DATE_TIME: item.time,
        REMARK: item.remark
      };
      tmpData.push(tmp);
    });
    this.excelSheet.exportAsExcelFile(tmpData, 'accountstatment');
  }

  get frmType() { return this.frm.get('type'); }
  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

}

