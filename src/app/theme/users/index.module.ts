import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { UsersComponent } from './index.component';
import { CreateComponent } from './client/create/index.component';
import { ManageComponent } from './client/manage/index.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      {
        path: 'client',
        children:[
          {
            path:'',
            component:ManageComponent
          },
          {
            path:'create',
            component:CreateComponent
          }
        ]
      },
      {
        path: 'action',
        loadChildren: './action/index.module#ActionModule'
      },
      {
        path: 'manage-system',
        loadChildren: './manage-system/index.module#ManageSystemModule'
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: [UsersComponent],
})
export class UsersModule {
}
