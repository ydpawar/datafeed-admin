import { Component, OnInit, AfterViewInit, OnDestroy, Injector } from '@angular/core';
import { ActionService, ClientService } from '../../../../_api/index';
import { AuthIdentityService, ScriptLoaderService, ToastrService } from '../../../../_services/index';
import { BaseComponent } from '../../../../common/commonComponent';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $, swal;

interface User {
    api_purchase_date: Date;
    api_renewal_date: Date;
    instrument_name:string;
    available: string;
    balance: string;
    expose: string;
    flag: string;
    id: string;
    isActive: string;
    isBlock: string;
    isLock: string;
    is_login: string;
    mc: string;
    name: string;
    no_of_element: string;
    pName: string;
    pl_balance: string;
    remark: string;
    total_payment: string;
    username: string;
}

class UserObj implements User {
    api_purchase_date: Date;
    api_renewal_date: Date;
    instrument_name:string;
    available: string;
    balance: string;
    expose: string;
    flag: string;
    id: string;
    isActive: string;
    isBlock: string;
    isLock: string;
    is_login: string;
    mc: string;
    name: string;
    no_of_element: string;
    pName: string;
    pl_balance: string;
    remark: string;
    total_payment: string;
    username: string;
}


@Component({
    selector: 'app-user',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
    providers: [ClientService, ActionService]
})
export class ManageComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

    page = { start: 1, end: 5 };
    user: any;
    aList: User[] = [];
    cItem: User;
    isEmpty = false;
    isLoading = false;
    dataTableId = 'DataTables_client';
    title = 'Client';
    createUrl = '/users/client/create';
    breadcrumb: any = [{ title: 'Client', url: '/' }, { title: 'Manage', url: '/' }];

    frm: FormGroup;
    client_name: string;

    constructor(
        inj: Injector,
        private service: ClientService,
        private service2: ActionService,
        private toaster: ToastrService,
        public formBuilder: FormBuilder,
        private loadJs: ScriptLoaderService) {
        super(inj);
        const auth = new AuthIdentityService();
        if (auth.isLoggedIn()) {
            this.user = auth.getIdentity();
        }
        this.createForm();
    }

    ngOnInit() {
        this.spinner.show();
        this.applyFilters();
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() {
        window.localStorage.removeItem(this.dataTableId);
    }

    applyFilters() {
        this.service.manage().subscribe((res) => this.onSuccess(res));
    }

    onSuccess(res) {

        if (res.status !== undefined && res.status === 1) {
            if (res.data !== undefined && res.data !== undefined) {
                $('#DataTables_client').dataTable().fnDestroy();
                const items = res.data;
                const data: User[] = [];

                if (items.length > 0) {
                    this.aList = [];
                    for (const item of items) {
                        const cData: User = new UserObj();

                        cData.api_purchase_date = item.api_purchase_date;
                        cData.api_renewal_date = item.api_renewal_date;
                        cData.instrument_name = item.instrument_name;
                        cData.available = item.available;
                        cData.balance = item.balance;
                        cData.expose = item.expose;
                        cData.flag = item.flag;
                        cData.id = item.id;
                        cData.isActive = item.isActive;
                        cData.isBlock = item.isBlock;
                        cData.isLock = item.isLock;
                        cData.is_login = item.is_login;
                        cData.mc = item.mc;
                        cData.name = item.name;
                        cData.no_of_element = item.no_of_element;
                        cData.pName = item.pName;
                        cData.pl_balance = item.pl_balance;
                        cData.remark = item.remark;
                        cData.total_payment = item.total_payment;
                        cData.username = item.username;

                        data.push(cData);
                    }
                } else {
                    this.isEmpty = true;
                }

                this.aList = data;
                this.page.end = this.page.end + items.length - 1;
                this.initDataTables('DataTables_client');
            }
        }
        this.spinner.hide();
    }

    doStatusUpdate(uid, typ) {
        swal.fire({
            title: 'Are you sure to change this status ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                if (typ === 1) {
                    this.service2.doBlockUnblock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
                } else {
                    this.service2.doLockUnlock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
                }
            }
        });
    }

    // doStatusDelete(uid) {
    //     swal.fire({
    //         title: 'Are you sure to delete ?',
    //         // text: 'Are you sure to logout?',
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes'
    //     }).then((result) => {
    //         if (result.value) {
    //             this.service2.doDelete(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
    //         }
    //     });
    // }

    // onSuccessStatusUpdate(res) {
    //     if (res.status === 1) {
    //         this.applyFilters();
    //     }
    // }

    doStatusDelete(uid) {
        swal.fire({
          title: 'Are you sure to delete ?',
          // text: 'Are you sure to logout?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.value) {
            this.onSuccessStatusUpdate(uid);
            }
        });
    }

    onSuccessStatusUpdate(uid) {
        this.service2.doDelete(uid).subscribe((res: any) => {
            if(res.status === 1) {
                this.toaster.success(res.message);
                this.applyFilters()
            } else {
                this.toaster.error('Somthing want to be wrong..!')
            }
        });
    }
    // setTitleOnClick(title) {
    //   window.localStorage.setItem('title', title);
    // }

    updateLots(item) {
        this.frm.reset();
        $('.clear-settlement').modal('show');
        $('body').removeClass('modal-open');
        this.client_name = item.name;
        this.frm.patchValue({
            uid: item.id,
            name: item.name,
            no_of_element: item.instrument_name,
            instrument_name: item.instrument_name,
            total_payment: item.total_payment,
            purchase_date: item.purchase_date,
            renewal_date: item.renewal_date,
        });
    }

    createForm() {
        this.frm = this.formBuilder.group({
            uid: [''],
            name: [''],
            no_of_element: [''],
            instrument_name: ['', Validators.required],
            total_payment: ['', Validators.required],
            purchase_date: ['', Validators.required],
            renewal_date: ['', Validators.required],
        });
    }

    submitForm() {
        if (this.frm.valid) {
            const data = this.frm.value;
            this.service.add(data).subscribe((res) => this.onSubmit(res));
        }
    }

    onSubmit(res) {
        this.toaster.success(res.message);
        if (res.status === 1) {
            this.applyFilters();
            $('.clear-settlement').modal('hide');
            $('body').removeClass('modal-open');
        }
    }

    get frmInstrument() { return this.frm.get('instrument_name'); }
    get frmPayment() { return this.frm.get('total_payment'); }
    get frmPurchase() { return this.frm.get('purchase_date'); }
    get frmRenewal() { return this.frm.get('renewal_date'); }
}

