import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClientComponent } from './index.component';
import {ManageComponent} from './manage/index.component';
import {ManageModule} from './manage/index.module';

import {CreateComponent} from './create/index.component';
import {CreateModule} from './create/index.module';

import {SettingComponent} from './setting/index.component';
import {SettingModule} from './setting/index.module';

import {ReferenceComponent} from './reference/index.component';
import {ReferenceModule} from './reference/index.module';



const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      {
        path: '',
        component: ManageComponent
      },
      {
        path: 'manage',
        component: ManageComponent
      },
      {
        path: 'reference/:uid',
        component: ReferenceComponent
      },
      {
        path: 'create',
        component: CreateComponent
      },
      {
        path: 'setting',
        component: SettingComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule,
    ManageModule, CreateModule, SettingModule, ReferenceModule
  ], exports: [
    RouterModule
  ], declarations: [ ClientComponent]
})
export class ClientModule {

}
