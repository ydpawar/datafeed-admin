import { Component, OnInit, AfterViewInit } from '@angular/core';
import {ClientService} from '../../../../_api/index';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AuthIdentityService} from '../../../../_services';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [ClientService]
})
export class CreateComponent implements OnInit, AfterViewInit {

  title = 'Client - Create';
  breadcrumb: any = [{title: 'Client', url: '/' }, {title: 'Create', url: '/' }];

  user: any;
  frm: FormGroup;
  userError: string;
  parentUser: string;
  plError: number;
  tmpplError: number;
  belance: number;
  tmpbelance: number;

  constructor(
      private formBuilder: FormBuilder,
      private service: ClientService,
      private router: Router,
      private route: ActivatedRoute,
      // tslint:disable-next-line:variable-name
      private _location: Location
  ) {
    const auth = new AuthIdentityService();

    // if (auth.isLoggedIn()) {
    //   this.user = auth.getIdentity();
    //   // tslint:disable-next-line:triple-equals
    //   if ( this.user.role == 'ADMIN' || this.user.role == 'ADMIN2') {
    //     // this._location.back();
    //   }
    // }
  }

  ngOnInit() {
    this.createForm();
    this.checkUserData();
  }

  ngAfterViewInit() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      name: ['', [ Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)] ],
      username: ['', [ Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)] ],
      password: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        Validators.pattern(new RegExp('^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})'))]],
      balance: ['', Validators.required],
      // mc: ['1', Validators.required],
      remark: ['']
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.create(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this._location.back();
      // this.router.navigate(['/manage']);
    }
  }

  checkUserData() {
    this.service.checkUserData().subscribe((res) => {
      this.intCheckUser(res);
    });
  }

  intCheckUser(res) {
    if (res.status === 1) {
      this.userError = '';
      // tslint:disable-next-line:triple-equals
      if ( res.data != undefined ) {
        this.belance = this.tmpbelance = res.data.balance;
        this.tmpplError = res.data.pl;
        this.parentUser = res.data.username;
      }
    }
    if (res.status === 0) {
      // tslint:disable-next-line:triple-equals
      if (res.success != undefined) {
        this.userError = res.success.message;
      }
    }
  }

  checkBalance(balance: any) {
    this.belance = this.tmpbelance - Number(balance.value);
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmName() { return this.frm.get('name'); }
  get frmUsername() { return this.frm.get('username'); }
  get frmPassword() { return this.frm.get('password'); }
  get frmBalance() { return this.frm.get('balance'); }
  // get frmMc() { return this.frm.get('mc'); }
  get frmRemark() { return this.frm.get('remark'); }

}

