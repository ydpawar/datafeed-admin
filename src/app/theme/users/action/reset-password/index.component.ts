import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {ActionService} from '../../../../_api/index';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AuthIdentityService} from '../../../../_services';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [ActionService]
})
export class ResetPasswordComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Users - Reset Password';
  breadcrumb: any = [{title: 'Users', url: '/' }, {title: 'Reset Password', url: '/' }];

  uid: string;
  frm: FormGroup;

  constructor(
      private formBuilder: FormBuilder,
      private service: ActionService,
      private router: Router,
      private route: ActivatedRoute,
      // private authIdentity: AuthIdentityService,
      // tslint:disable-next-line:variable-name
      private _location: Location
  ) {
    this.uid = this.route.snapshot.params.uid;
    // tslint:disable-next-line:triple-equals
    if ( window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined ) {
      this.title = 'Reset Password - ' + window.localStorage.getItem('title');
    }
  }

  ngOnInit() {
    this.createForm();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem('title');
  }

  createForm() {
    this.frm = this.formBuilder.group({
      uid: [this.uid],
      password: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        Validators.pattern(new RegExp('^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})'))]],
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.resetPassword(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this._location.back();
      // this.authIdentity.logOut(this.router);
    }
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmPassword() { return this.frm.get('password'); }

}

