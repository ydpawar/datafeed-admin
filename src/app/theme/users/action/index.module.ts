import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ActionComponent } from './index.component';

import {ResetPasswordComponent} from './reset-password/index.component';
import {ResetPasswordModule} from './reset-password/index.module';

import {ChangePasswordComponent} from './change-password/index.component';
import {ChangePasswordModule} from './change-password/index.module';

import {DepositComponent} from './deposit/index.component';
import {DepositModule} from './deposit/index.module';

import {WithdrawalComponent} from './withdrawal/index.component';
import {WithdrawalModule} from './withdrawal/index.module';


const routes: Routes = [
  {
    path: '',
    component: ActionComponent,
    children: [
      {
        path: 'reset-password/:uid',
        component: ResetPasswordComponent
      },
      {
        path: 'change-password/:uid',
        component: ChangePasswordComponent
      },
      {
        path: 'deposit/:uid',
        component: DepositComponent
      },
      {
        path: 'withdrawal/:uid',
        component: WithdrawalComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule,
    ResetPasswordModule, DepositModule, WithdrawalModule, ChangePasswordModule
  ], exports: [
    RouterModule
  ], declarations: [ ActionComponent]
})
export class ActionModule {

}
