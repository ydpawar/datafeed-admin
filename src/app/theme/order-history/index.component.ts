import { AfterViewInit, Component, OnDestroy, OnInit, Injector } from '@angular/core';
import { HistoryService } from '../../_api';
import { Validators } from '@angular/forms';
import { BaseComponent } from '../../common/commonComponent';
import { ExcelService } from '../../common/excel.service';

declare var $;

// tslint:disable-next-line:class-name
interface listData {
    api_purchase_date: string,
    api_renewal_date: string,
    id: string,
    name: string,
    no_of_element: string,
    status: number,
    total_payment: string,
    uid: string,
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
    api_purchase_date: string;
    api_renewal_date: string;
    id: string;
    name: string;
    no_of_element: string;
    status: number;
    total_payment: string;
    uid: string;
}

@Component({
    selector: 'app-home',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
    providers: [HistoryService]
})

export class OrderHistoryComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    title = 'History';
    breadcrumb: any = [{ title: 'Order History', url: '/' }];
    page = { start: 1, end: 5 };
    uid = '';
    aList: listData[] = [];
    cItem: listData;
    isEmpty = false;
    isLoading = false;
    dataTableId = 'DataTables_current_bet';

    constructor(
        inj: Injector,
        private service: HistoryService,
        private excelSheet: ExcelService
    ) {
        super(inj);
        this.uid = this.activatedRoute.snapshot.params.id;
        // tslint:disable-next-line:triple-equals
        if (window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined) {
            this.title = 'History for ' + window.localStorage.getItem('title');
        }
    }

    ngOnInit() {
        this.spinner.show();
        this.createForm();
        this.applyFilters();
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() {
        window.localStorage.removeItem('title');
        window.localStorage.removeItem(this.dataTableId);
    }

    applyFilters() {
        this.service.ClientHistory(this.uid, {}).subscribe((res) => this.onSuccess(res));
    }

    onSuccess(res) {
        this.spinner.hide();
        if (res.status !== undefined && res.status === 1) {
            $('#DataTables_current_bet').dataTable().fnDestroy();
            if (res.userName !== undefined) {
                this.title = 'History for ' + res.userName;
            }
            if (res.data !== undefined) {
                const items = res.data;
                const data: listData[] = [];

                if (items.length > 0) {
                    this.aList = [];
                    for (const item of items) {
                        const cData: listData = new listDataObj();

                        cData.id = item.id;
                        cData.uid = item.uid;
                        cData.total_payment = item.total_payment;
                        cData.name = item.name;
                        cData.no_of_element = item.no_of_element;
                        cData.api_purchase_date = item.api_purchase_date;
                        cData.api_renewal_date = item.api_renewal_date;
                        cData.status = item.status;

                        data.push(cData);
                    }
                } else {
                    this.isEmpty = true;
                }

                this.aList = data;
                this.page.end = this.page.end + items.length - 1;
                // this.loadJs.load('script' , 'assets/js/datatables.init.js');
                this.loadScript();

            }
        }
    }

    createForm() {
        this.frm = this.formBuilder.group({
            type: ['', Validators.required],
            start: ['', Validators.required],
            end: ['', Validators.required],
        });
    }

    submitForm(fType = null) {
        if (fType != null) {
            this.spinner.show();
            const data = this.frm.value;
            data.ftype = fType;
            this.service.bets(this.uid, data).subscribe((res) => this.onSearch(res));
        } else {
            if (this.frm.valid) {
                this.spinner.show();
                const data = this.frm.value;
                this.service.bets(this.uid, data).subscribe((res) => this.onSearch(res));
            }
        }
    }

    onSearch(res) {
        if (res.status === 1) {
            // this.frm.reset();
            this.onSuccess(res);
        }
    }

    loadScript() {
        // tslint:disable-next-line:only-arrow-functions
        $(document).ready(function () {
            $('#DataTables_current_bet').DataTable({
                'scrollX': true,
                'destroy': true,
                'retrieve': true,
                'stateSave': true,
                'language': {
                    'paginate': {
                        'previous': '<i class=\'mdi mdi-chevron-left\'>',
                        'next': '<i class=\'mdi mdi-chevron-right\'>'
                    }
                },
                'drawCallback': function drawCallback() {
                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                }
            }); // Complex headers with column visibility Datatable
        });

    }

    excelDownload() {
        const tmpData = [];
        let tmp = {
            ORDER_ID: '',
            CLIENT_ID: '',
            CLIENT_NAME: '',
            TOTAL_PAYMENT: '',
            NO_OF_ELEMENT: '',
            PURCHASE_DATE: '',
            RENEWAL_DATE: '',
        };
        tmpData.push(tmp);
        this.aList.forEach((item, index) => {
            tmp = {
                ORDER_ID: item.id,
                CLIENT_ID: item.uid,
                CLIENT_NAME: item.name,
                TOTAL_PAYMENT: item.total_payment,
                NO_OF_ELEMENT: item.no_of_element,
                PURCHASE_DATE: item.api_purchase_date,
                RENEWAL_DATE: item.api_renewal_date,
            };
            tmpData.push(tmp);
        });
        this.excelSheet.exportAsExcelFile(tmpData, 'bet-list');
    }

    get frmType() { return this.frm.get('type'); }
    get frmStart() { return this.frm.get('start'); }
    get frmEnd() { return this.frm.get('end'); }

}
