import {AfterViewInit, Component, OnDestroy, OnInit, Injector} from '@angular/core';
import {SummaryService} from '../../_api';
import {AuthIdentityService} from '../../_services';
import { Validators} from '@angular/forms';
import {BaseComponent} from '../../common/commonComponent';

declare var $;
// tslint:disable-next-line:class-name
interface userData {
  id: string;
  pid: string;
  name: string;
  role: string;
  pl: string;
}

// tslint:disable-next-line:class-name
class userDataObj implements userData {
  id: string;
  pid: string;
  name: string;
  role: string;
  pl: string;
}

declare var $;

@Component({
  selector: 'app-home',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SummaryService]
})

export class SettlementComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  page = { start: 1, end: 5 };
  title = 'Settlement';
  breadcrumb: any = [{title: 'Settlement', url: '/' }];
  uid = '';
  user: any;
  aListTotal: any;
  aList: userData[] = [];
  aList1Total: any;
  aList1: userData[] = [];
  cItem: userData;
  isEmpty = false;
  isLoading = false;
  cUserName: string;
  pUserId: string;
  SettleUser: string;
  SettleType: string;
  errorSummary: string;
  userBalance:number = 0;
  isSummerySubmit: boolean = false;
  isBack: boolean = false;
  isCanSettle: boolean = false;
  isCheckBox: boolean = false;
  isErrorShow: boolean = false;

  // tslint:disable-next-line:max-line-length
  constructor(
      inj: Injector,
      private service: SummaryService,
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }

  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  applyFilters() {
    this.service.plusUsers(this.uid).subscribe((res) => this.onSuccessPlus(res));
    this.service.minusUsers(this.uid).subscribe((res) => this.onSuccessMinus(res));
  }

  onSuccessPlus(res) {

    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        $('#ScrollVerticalPlus').dataTable().fnDestroy();
        const items = res.data;
        this.aListTotal = res.totalData;

        const data: userData[] = [];
        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: userData = new userDataObj();
            cData.id = item.uid;
            cData.pid = item.pid;
            cData.name = item.name;
            cData.role = item.role;
            cData.pl = item.pl;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.loadScript();
        this.spinner.hide();
      }
    }
  }

  onSuccessMinus(res) {
    if (res.status !== undefined && res.status === 1) {
      $('#ScrollVerticalMinus').dataTable().fnDestroy();
      if (res.cUserData !== undefined) {
        this.cUserName = res.cUserData.cUserName;
        this.pUserId =  res.cUserData.pid;
        this.isBack =  res.cUserData.isBack;
        this.isCanSettle = res.cUserData.isCanSettle;
      }

      if (res.data !== undefined ) {
        const items = res.data;
        this.aList1Total = res.totalData;
        const data: userData[] = [];
        if (items.length > 0) {
          this.aList1 = [];
          for (const item of items) {
            const cData: userData = new userDataObj();
            cData.id = item.uid;
            cData.name = item.name;
            cData.role = item.role;
            cData.pl = item.pl;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList1 = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.loadScript();
      }
    }
  }

  goToUserSummary(usr) {
    this.service.plusUsers(usr.id).subscribe((res) => this.onSuccessPlus(res));
    this.service.minusUsers(usr.id).subscribe((res) => this.onSuccessMinus(res));
  }

  goToUserSummaryBack(pid) {
    this.service.plusUsers(pid).subscribe((res) => this.onSuccessPlus(res));
    this.service.minusUsers(pid).subscribe((res) => this.onSuccessMinus(res));
  }

  clearSettlement(item, typ) {
    this.frm.reset(); this.userBalance = 0;
    this.isErrorShow = false; this.isCheckBox = false;
    if ( item.role === 'C' ) {
      this.isCheckBox = true;
    }

    this.isSummerySubmit = false;

    $('.clear-settlement').modal('show');
    $('body').removeClass('modal-open');

    this.SettleUser = item.name;
    this.SettleType = typ;

    this.frm.patchValue({
      amount: item.pl,
      tempamount: item.pl,
      type: typ,
      uid: item.id
    });

  }

  createForm() {
    this.frm = this.formBuilder.group({
      amount: ['', Validators.required],
      remark: ['', Validators.required],
      tempamount: [''],
      type: [''],
      uid: [''],
      checkbox: [''],
    });
  }

  submitForm() {
    if (this.frm.valid && this.isSummerySubmit === false && !this.isErrorShow ) {
      const data = this.frm.value; this.isSummerySubmit = true;
      this.service.doSettlement(this.uid, data).subscribe((res) => this.onSubmit(res));
    }
  }

  onSubmit(res) {
    if (res.status === 1) {
      document.getElementById('close-settlement').click();
      this.applyFilters();
    } else {
      this.isErrorShow = true;
      this.errorSummary = res.error.message;
    }
  }

  settlement(val) {
    // debugger;
    const tt: any = this.frm.get('tempamount');
    if (val.value !== '') {
      // tslint:disable-next-line:radix
      this.userBalance = parseInt(tt.value) - parseInt(val.value);
    } else {
      // tslint:disable-next-line:radix
      this.userBalance = parseInt(tt.value);
    }
    if ( this.userBalance < 0 ) { this.isErrorShow = true; } else { this.isErrorShow = false; }

  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#ScrollVerticalPlus, #ScrollVerticalMinus').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "paging": false
      }); // Complex headers with column visibility Datatable
    });

  }

  get frmAmount() { return this.frm.get('amount'); }
  get frmRemark() { return this.frm.get('remark'); }

}
