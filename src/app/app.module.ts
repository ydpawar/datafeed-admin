import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { HttpService, ScriptLoaderService } from './_services/index';
import { ToastrService } from './_services/toastr.service';
import {AuthGuard, AuthGuardChild} from './_guards/auth.guard';

import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './public/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HrefPreventDefaultDirective} from './_directives/href-prevent-default.directive';
import {SafePipe} from './_directives/safe.pipe';
import {UnwrapTagDirective} from './_directives/unwrap-tag.directive';
import { RecaptchaModule } from 'ng-recaptcha';
import { BaseComponent } from './common/commonComponent';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    loadChildren: './theme/theme.module#ThemeModule',
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BaseComponent,
    SafePipe,
    HrefPreventDefaultDirective,
    UnwrapTagDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    RecaptchaModule
  ],
  providers: [HttpService, ToastrService, AuthGuard, AuthGuardChild, ScriptLoaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
