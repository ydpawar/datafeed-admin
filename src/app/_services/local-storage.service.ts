/**
 * New typescript file
 */
import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorage {
    setItem(key: string, value: string) {
        try{
            localStorage.setItem(key, value);
        }catch( e ){
            console.log( "LocalStorage Error" , e );
        }
    }

    getItem(key: string): string | null {
        return localStorage.getItem(key);
    }

    removeItem(key: string): void {
        localStorage.removeItem(key);
    }
}
