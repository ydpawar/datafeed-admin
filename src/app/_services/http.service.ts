import {Injectable} from '@angular/core';
// import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import {HttpHeaders, HttpClient, HttpResponse, HttpInterceptor, HttpParams} from '@angular/common/http';
import {Observable, Scheduler, TimeoutError, SchedulerLike, ObservableInput, observable, throwError} from 'rxjs';
import {AuthIdentityService, User} from './auth-identity.service';
import {map, catchError, tap, timeout, timeoutWith, delay} from 'rxjs/operators';

import * as CryptoJS from 'crypto-js';
import {Router} from '@angular/router';
import {ToastrService} from './toastr.service';

declare var $, toastr;

export class HttpRequestProcessDisplay {
  loader: boolean;
  loaderMessage: string;
  responseMessage: boolean;
}

export class HttpRequestModel {
  header: string;
  url: string;
  method: string;
  params: any;
  body: any;
  headersContentType?: any;
}

export class RequestOption {
  header: string;
  url: string;
  method: string;
  params: any;
  body: any;
  headers: any;
  headersContentType?: any;
}

@Injectable()
export class HttpService {
  enableEncryption: string = 'false';

  baseUrl = 'http://52.66.104.39/ApiserviceAdmin/public/api/';

  nodeUrl = 'http://18.216.100.103:3000/dev/';

  // TODO updated by guest

  display: HttpRequestProcessDisplay;

  constructor(private http: HttpClient, private router: Router, private _toastr: ToastrService) {
    window[ 'operatorId' ] = 1; // 1 for dream
  }

  /**
   * Before any Request.
   */
  private beforeRequest(): void {
    if (this.display.loader) {
      var message = 'Please Wait...';

      if (this.display.loaderMessage !== '') {
        message = this.display.loaderMessage;
      }

    }
  }

  /**
   * After any request.
   */
  private afterRequest(): void {
    if (this.display.loader) {
      //  mApp.unblockPage();
    }
  }

  /**
   * Request options.
   * @param options
   * @returns {RequestOptionsArgs}
   */
  private requestOptions(httpRequest: HttpRequestModel, options?: RequestOption): RequestOption {
    let rData = httpRequest.body;


    let headers = {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache, no-store',
      'encryption': this.enableEncryption,
      // 'language': cLang,
      // 'apiversion': '2.0',
    };

    options = new RequestOption();

    if (httpRequest.header === 'Auth') {
      try {
        let auth = new AuthIdentityService();

        if (auth.isLoggedIn()) {
          let user = auth.getIdentity();
          headers['Authorization'] = 'Bearer ' + user.token;
        }else {
          auth.logOut(this.router);
          this._toastr.error('Your Session has been expired, please login again.', 'Logout');
        }
      } catch (e) {

      }

    } else {

    }

    let body = {};
    if (this.enableEncryption === 'true') {
      body = {data: this.encryptData(rData)};
    } else {
      body = rData;
    }
    options.body = body;
    options.params = httpRequest.params;
    options.method = httpRequest.method;
    options.headers = new HttpHeaders(headers);

    return options;
  }


  private encryptData(rData) {
    const key = CryptoJS.enc.Utf8.parse('ca0761c0b720b9ce1c93183d8f03d854');
    const iv = CryptoJS.enc.Utf8.parse('9c3243c35e6cb118');
    const data = JSON.stringify(rData);
    const options = {keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7};

    const encrypted = CryptoJS.AES.encrypt(data, key, options);
    console.log(encrypted);

    return encrypted.toString();
  }

  init(httpRequest: HttpRequestModel, display: HttpRequestProcessDisplay): Observable<any> {
    this.display = display;
    let options = this.requestOptions(httpRequest);

    if (httpRequest.body !== undefined && httpRequest.body.status !== undefined && httpRequest.body.status === true) {
      httpRequest.body = '';
      return this.request(httpRequest.url, options);
    }
    return this.request(this.baseUrl + httpRequest.url, options);
  }

  initNode(httpRequest: HttpRequestModel, display: HttpRequestProcessDisplay): Observable<any> {
    this.display = display;
    let options = this.requestOptions(httpRequest);

    if (httpRequest.body !== undefined && httpRequest.body.status !== undefined && httpRequest.body.status === true) {
      httpRequest.body = '';
      return this.request(httpRequest.url, options);
    }
    return this.request(this.nodeUrl + httpRequest.url, options);
  }

  request(url: string, option: RequestOption): Observable<any> {
    this.beforeRequest();
    return this.http.request(option.method, url, option)
      .pipe(
        timeout(30000),
        map((response: any) => this.mapResponse(response)),
        catchError((err: any, cught: any) => this.onCatch(err, cught)),
        tap(
          (res: any) => {
            return this.onSuccess(res);
          },
          (error: any) => {
            this.onError(error);
          }
        )
      );
  }

  private mapResponse(res: string): string {
    if (res['data'] !== undefined && this.enableEncryption === 'true') {
      res['data'] = this.decryptData(res['data']);
    }
    return res;
  }

  private onRequestTimeOut(): ObservableInput<{}> {

    return [new Observable((observable) => {
      return observable;
    })];
  }

  /*
   * onSuccess
   * @param res
   */

  private onSuccess(res: any) {
    this.afterRequest();
    //debugger;
    if (res['error'] !== undefined) {
      if (res['error']['httpStatus'] === 503) {
        let auth = new AuthIdentityService();
        auth.logOut(this.router);
        this._toastr.error('Your Session has been expired, please login again.', 'Logout');
      }
    }

    if (res['status'] === 1) {
      if (this.display.responseMessage) {
        let msg = 'Success!';

        if (res["success"] !== undefined && res["success"]["message"] != undefined) {
          msg = res["success"]["message"];
        }

        var content;
        if (res['error'] !== undefined && res['error']['message'] !== undefined) {
          this._toastr.error(res['error']['message'], 'Error');
        } else {
          this._toastr.success(msg, 'success');
        }
      }
    } else if (res['api_status'] === 402 ) {
      let msg = 'Unauthorize Access!';
      if (res['error'] !== undefined && res['error']['message'] !== undefined) {
        msg = res['error']['message'];
      }
      let auth = new AuthIdentityService();
      auth.logOut(this.router);
      this._toastr.error(msg, 'Unauthorize Access!');
    } else {
      let msg = 'Something went wrong!';
      if (res.error !== undefined) {
        msg = res.error.message;
      }
      if (this.display.responseMessage) {
        this._toastr.error(msg, 'Error');
      }
    }

    return res;
  }

  private decryptData(res: string): any {
    const key = CryptoJS.enc.Utf8.parse('ca0761c0b720b9ce1c93183d8f03d854');
    const ivv = CryptoJS.enc.Utf8.parse('9c3243c35e6cb118');

    const decrypted = CryptoJS.AES.decrypt(res, key, {keySize: 128 / 8, iv: ivv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7});

    return JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
  }

  private onCatch(error: any, caught: Observable<any>): Observable<any> {
   // debugger;
    this.afterRequest();
    return throwError(error);
  }

  /**
   * onError
   * @param error
   */
  private onError(error: any) {
    // In a real world app, you might use a remote logging infrastructure

    let errMsg: string;
   // debugger;
    if (error instanceof HttpResponse) {
      const body = error;
      const err = body.status === 2 ? body : JSON.stringify(body);

      // this.displayMessage('error', err);
      // $.notify(err, 'danger');

      return throwError(err);
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    // this.displayMessage('error', errMsg);
    // $.notify(errMsg, 'danger');

    return throwError(errMsg);
  }


}
