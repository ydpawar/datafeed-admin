import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpRequestModel, HttpRequestProcessDisplay, RequestOption, HttpService } from './../../_services/index';

@Injectable()
export class HistoryService {
  constructor(private http: HttpService) { }

    // history Api

  ClientHistory(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/history/' + id;
    } else {
      httpRequestModel.url = 'transaction/history';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  // history Api

  chips(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'history/chip-history/' + id;
    } else {
      httpRequestModel.url = 'history/chip-history';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  bets(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'current-order/' + id;
    } else {
      httpRequestModel.url = 'history/bet-history';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  doBetDelete(betId): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'dashboard/event/bet-delete/' + betId;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  marketBets(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    httpRequestModel.url = 'dashboard/market-bet-list';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  systemMarketBets(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    httpRequestModel.url = 'dashboard/system-market-bet-list';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  profitLoss(id): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/profit-loss/' + id;
    } else {
      httpRequestModel.url = 'transaction/profit-loss';
    }
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  profitLossEvent(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/profit-loss/by-event/' + id;
    } else {
      httpRequestModel.url = 'transaction/profit-loss/by-event';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  profitLossMarket(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/profit-loss/by-market/' + id;
    } else {
      httpRequestModel.url = 'transaction/profit-loss/by-market';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  teenpattiProfitLoss(id): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    if (id) {
      httpRequestModel.url = 'transaction/teenpatti-profit-loss/' + id;
    } else {
      httpRequestModel.url = 'transaction/teenpatti-profit-loss';
    }
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  accountStatement(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    if (id) {
      httpRequestModel.url = 'transaction/account-statement/' + id;
    } else {
      httpRequestModel.url = 'transaction/account-statement';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

}
