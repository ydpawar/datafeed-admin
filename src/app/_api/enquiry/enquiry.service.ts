import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { HttpRequestModel, HttpRequestProcessDisplay, HttpService } from './../../_services/index';
@Injectable({
  providedIn: 'root'
})
export class EnquiryService {

  constructor(
    private http:HttpService
  ) { }

  getEnquriy(): Observable<JSON> {
      const httpRequestModel = new HttpRequestModel();
      httpRequestModel.url = 'client/enquiry';
      httpRequestModel.method = 'GET';
      httpRequestModel.body = {};
      httpRequestModel.header = 'Auth';
  
      const processDisplay = new HttpRequestProcessDisplay();
      processDisplay.loader = true;
      processDisplay.loaderMessage = 'Loading Data!';
      processDisplay.responseMessage = false;
  
      return this.http.init(httpRequestModel, processDisplay);
    }

    equiryFilter(uid,Data){
      const httpRequestModel = new HttpRequestModel();
      httpRequestModel.url = 'client/enquiry';
      httpRequestModel.method = 'POST';
      httpRequestModel.body = {};
      httpRequestModel.header = 'Auth';
  
      const processDisplay = new HttpRequestProcessDisplay();
      processDisplay.loader = true;
      processDisplay.loaderMessage = 'Loading Data!';
      processDisplay.responseMessage = true;
  
      return this.http.init(httpRequestModel, processDisplay);
    }

    deleteEnquiry(id){
      const httpRequestModel = new HttpRequestModel();
      httpRequestModel.url = 'delete/enquiry';
      httpRequestModel.method = 'POST';
      httpRequestModel.body = {id};
      httpRequestModel.header = 'Auth';
  
      const processDisplay = new HttpRequestProcessDisplay();
      processDisplay.loader = true;
      processDisplay.loaderMessage = 'Loading Data!';
      processDisplay.responseMessage = true;
  
      return this.http.init(httpRequestModel, processDisplay);
    }
}
