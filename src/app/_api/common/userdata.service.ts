import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


import { HttpRequestModel, HttpRequestProcessDisplay, HttpService } from './../../_services/index';

@Injectable()
export class UserDataService {
  constructor(private http: HttpService) { }

  getUserData(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'dashboard/get-user-data';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  getSystemdata(id): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'operator-detail?operatorId=' + id;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'No Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = false;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }


}
