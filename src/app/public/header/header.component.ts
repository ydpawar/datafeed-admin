import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoginService, UserDataService} from '../../_api/index';
import {AuthIdentityService, ToastrService} from './../../_services/index';
import {Router} from '@angular/router';
import swal from 'sweetalert2';

declare var $;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    providers: [LoginService, UserDataService, AuthIdentityService]
})
export class HeaderComponent implements OnInit, OnDestroy {

    userData: any;
    systemId: string;
    public setlogo: string;
    private isDestroy: boolean = false;

    // tslint:disable-next-line:max-line-length
    constructor( private service: UserDataService,
                 private auth: LoginService,
                 private route: Router,
                 private authIdentity: AuthIdentityService,
                 private toaster: ToastrService) {

    }

    ngOnDestroy() {
        this.isDestroy = true;
    }
    ngOnInit() {
        this.systemId = window[ 'operatorId' ];
        this.getUserData();
    }

    async getUserData() {
        await this.service.getUserData().subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessUserData(data);
            },
            error => {
                this.toaster.error(error.message, 'Something want wrong..!');
            });
    }

    onSuccessUserData(response) {
        if (response.status !== undefined) {
            if (response.status === 1) {
                this.userData = response.data;
                window.localStorage.setItem('commentary', response.data.commentary);
                window.localStorage.setItem('lastcomm', response.data.lastcomm);

                if (!this.isDestroy) {
                    const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getUserData(); }, 15000);
                }
            }
        }
    }

    logout() {
        swal.fire({
            title: 'Are you sure to logout?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.auth.doLogout().subscribe((res) => this.onSuccessLogout(res));
            }
        });
    }

    onSuccessLogout(res) {
        this.authIdentity.logOut(this.route);
    }

}
