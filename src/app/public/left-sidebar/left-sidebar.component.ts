import { Component, OnInit } from '@angular/core';
import { MenuService} from '../../_api';
import { Router} from '@angular/router';
import { AuthIdentityService, ToastrService} from '../../_services';
import swal from 'sweetalert2';

declare  var $;
@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.css']
})
export class LeftSidebarComponent implements OnInit {

  mainMenuList: any;
  onLineClient: string;
  user: any;

  // tslint:disable-next-line:max-line-length
  constructor(private service: MenuService, private route: Router, private authIdentity: AuthIdentityService, private toaster: ToastrService) {
    const auth = new AuthIdentityService();

    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    //  console.log(this.user);
    }
  }

  ngOnInit() {
    this.getMainMenuData();
  }

  async getMainMenuData() {
    await this.service.getList().subscribe(
      // tslint:disable-next-line:no-shadowed-variable
      (data) => {
        this.onSuccess(data);
      },
      error => {
        this.toaster.error(error.message, 'Something want wrong..!');
      });
  }

  onSuccess(response) {
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.mainMenuList = response.data.list;
        this.onLineClient = response.data.onLineClient;
      }
    }
  }

  allUserLogout() {
    swal.fire({
      title: 'Are you sure to want all user logout?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.allUserLogout().subscribe((res) => this.onSuccessUserLogout(res));
      }
    });
  }

  onSuccessUserLogout(res) {
    if (res.status === 1) {
      this.getMainMenuData();
    }
  }

  closeMenu() {
    $('.topnav-menu').click();
  }

  changeClass(id, title, selectedId) {
    $('.activemenu').removeClass('active');
    $('#' + selectedId).addClass('active');
    if (title === 'Users') {
        $('.inActivegroup').toggle();
        $('.reportsgroup').hide();
        $('.manage-system').hide();
    } else if (title === 'Reports') {
        $('.reportsgroup').toggle();
        $('.inActivegroup').hide();
        $('.manage-system').hide();
    } else if (title === 'White Label') {
        $('.manage-system').toggle();
        $('.inActivegroup').hide();
        $('.reportsgroup').hide();
    } else {
        $('.reportsgroup').hide();
        $('.inActivegroup').hide();
        $('.manage-system').hide();
    }
  }

  changeSubClass(selectedId) {
    $('.activemenu').removeClass('active');
    $('#' + selectedId).addClass('active');
  }


}


