import { Component, OnInit, OnChanges} from '@angular/core';
import { LoginService, UserDataService } from '../../_api/index';
import { Router } from '@angular/router';
import { AuthIdentityService, ToastrService } from './../../_services/index';

declare var $;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthIdentityService, UserDataService]
})

export class LoginComponent implements OnInit{
  model: any = {};
  public setlogo: string;
  returnUrl: string;
  systemId: number;
  apkUrl: string;
  authId: string;
  passType = 'password';
  passType1 = 'password';
  passType2 = 'password';

  dataList: any ;
  eventList: any = [];
  marketList: any = [];
  isEmpty = true;

  your_system_active_varibale: string;
  your_system_message_varibale: string;

  recaptchaKey: string;

  constructor(private auth: LoginService,
    private service: UserDataService,
    private route: Router,
    private authIdentity: AuthIdentityService,
    private toastr: ToastrService
  ) {
    this.your_system_active_varibale = '1';
  }

  ngOnInit() {
    this.maintenance(this.dataList);

    this.systemId = Number(window['operatorId']);
  }
  

maintenance(response) {
    this.auth.maintenance(response).subscribe(
      (data) => {
        this.onSuccessDataList(data);
      },
      error => {
        this.toastr.error(error.message, 'Something want wrong..!');
      });
  }

onSuccessDataList(response) {

    response.data.forEach((item) => {
      if (item.key_name === 'IS_MAINTENANCE') {
        this.your_system_active_varibale = item.value;
      }if(item.key_name != 'IS_MAINTENANCE'){
        this.your_system_active_varibale = undefined;
      }if (item.key_name === 'MAINTENANCE_SETTING') {
        this.your_system_message_varibale = item.value;
      }
    });
  }

doLogin() {
  const data = {
    username: this.model.username,
    password: this.model.password,
    systemId: this.systemId,
    recaptchaKey: this.recaptchaKey
  };

  this.auth.doLogin(data).subscribe(
    // tslint:disable-next-line:no-shadowed-variable
    (data) => { this.intSuccessLogin(data); },
    error => {
      // tslint:disable-next-line:triple-equals
      this.toastr.error(error.message, 'Something want to wrong..!');

    });
}

intSuccessLogin(response) {
  if (response.status !== undefined && response.status === 1) {
    if (response.data !== undefined) {
      if (response.status === 1) {
        this.authIdentity.setUser(response.data);
        // window.localStorage.setItem('loadBanner', 'yes');
        this.route.navigate(['/']);
      } else {
        this.authId = response.data.auth_id;
        $('#login-form').hide();
        $('#change-pass-form').show();
      }

    } else {
      // this.toastr.error(response.error.message, 'Something want wrong..!');
    }
  }
}

dochangepass() {
  const data = {
    username: this.model.username,
    password: this.model.password,
    password1: this.model.password1,
    password2: this.model.password2,
  };

  this.auth.dochangepass(data).subscribe(
    // tslint:disable-next-line:no-shadowed-variable
    (data) => { this.intSuccesschangepass(data); },
    error => {
      // tslint:disable-next-line:triple-equals
      this.toastr.error(error.message, 'Something want to wrong..!');

    });
}

intSuccesschangepass(response) {
  if (response.status !== undefined && response.status === 1) {
    $('#login-form').show();
    $('#change-pass-form').hide();
  }
}

resolved(captchaResponse: string) {
  this.recaptchaKey = captchaResponse;
}
}
