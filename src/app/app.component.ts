import {Component, OnInit, AfterViewInit} from '@angular/core';
import {ScriptLoaderService} from './_services/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'Metagold';

  constructor(private script: ScriptLoaderService) {
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
   // this.script.load('script', 'assets/js/vendor.js');
  }
}
