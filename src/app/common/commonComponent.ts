import {Component, OnInit, PLATFORM_ID, Injector, NgZone, APP_ID} from '@angular/core';
import {TransferState, makeStateKey, Title, Meta} from '@angular/platform-browser';
import {isPlatformBrowser, isPlatformServer} from '@angular/common';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../environments/environment';
import {NgxSpinnerService} from 'ngx-spinner';
import swal from 'sweetalert2';
import {Location} from '@angular/common';

declare var jquery: any;
declare var $: any;

@Component({
    selector: 'parent-comp',
    template: ``,
    providers: []
})

export class BaseComponent {

    public activatedRoute: ActivatedRoute;
    public routeUrl: any;
    public titleService: Title;
    public metaService: Meta;
    public platformId: any;
    public appId: any;
    public router: Router;
    public baseUrl;
    public spinner: NgxSpinnerService;
    public formBuilder: FormBuilder;
    public frm: FormGroup;
    public backToLocation: Location;

    constructor(injector: Injector) {
        this.router = injector.get(Router);
        this.platformId = injector.get(PLATFORM_ID);
        this.appId = injector.get(APP_ID);
        this.titleService = injector.get(Title);
        this.metaService = injector.get(Meta);
        this.activatedRoute = injector.get(ActivatedRoute);
        this.spinner = injector.get(NgxSpinnerService);
        this.formBuilder = injector.get(FormBuilder);
        this.backToLocation = injector.get(Location);
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.routeUrl = event.urlAfterRedirects;
            }
        });
    }

    // *************************************************************//
    // @Purpose : To check server or browser

    // *************************************************************//
    isBrowser() {
        if (isPlatformBrowser(this.platformId)) {
            return true;
        } else {
            return false;
        }
    }

    // *************************************************************//
    // @Purpose : We can use following function to use localstorage
    // *************************************************************//
    setToken(key, value) {
        if (isPlatformBrowser(this.platformId)) {
            sessionStorage.setItem(key, value);
        }
    }

    getToken(key) {
        if (isPlatformBrowser(this.platformId)) {
            return sessionStorage.getItem(key);
        }
    }

    removeToken(key) {
        if (isPlatformBrowser(this.platformId)) {
            sessionStorage.removeItem(key);
        }
    }

    clearToken() {
        if (isPlatformBrowser(this.platformId)) {
            sessionStorage.clear();
        }
    }

    // *************************************************************//

    // *************************************************************//
    // @Purpose : We can use following function to use Toaster Service.
    // *************************************************************//
    popToast(type, title) {
        swal.fire({
            position: 'center',
            // type: type,
            text: title,
            showConfirmButton: false,
            timer: 3000,
            // customClass: 'custom-toaster'
        });
    }

    back() {
        this.backToLocation.back();
    }

    initDataTables(domElId) {
        // tslint:disable-next-line:only-arrow-functions
        $(document).ready(function() {
            $('#' + domElId).DataTable({
                'scrollX': true,
                'destroy': true,
                'retrieve': true,
                'stateSave': true,
                'language': {
                    'paginate': {
                        'previous': '<i class=\'mdi mdi-chevron-left\'>',
                        'next': '<i class=\'mdi mdi-chevron-right\'>'
                    }
                },
                'drawCallback': function drawCallback() {
                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                }
            }); // Complex headers with column visibility Datatable
        });
    }

    /****************************************************************************
     @PURPOSE      : To restrict or allow some values in input.
     @PARAMETERS   : $event
     @RETURN       : Boolen
     ****************************************************************************/
    RestrictSpace(e) {
        if (e.keyCode == 32) {
            return false;
        } else {
            return true;
        }
    }

    AllowNumbers(e) {
        var input;
        if (e.metaKey || e.ctrlKey) {
            return true;
        }
        if (e.which === 32) {
            return false;
        }
        if (e.which === 0) {
            return true;
        }
        if (e.which < 33) {
            return true;
        }
        if (e.which === 43 || e.which === 45) {
            return true;
        }
        if (e.which === 36 || e.which === 35) {
            return true;
        }
        if (e.which === 37 || e.which === 39) {
            return true;
        }
        input = String.fromCharCode(e.which);
        return !!/[\d\s]/.test(input);
    }

    AllowChar(e) {
        if ((e.keyCode > 64 && e.keyCode < 91) || (e.keyCode > 96 && e.keyCode < 123) || e.keyCode == 8) {
            return true;
        } else {
            return false;
        }
    }

    /****************************************************************************/
    /****************************************************************************/

    /****************************************************************************/
    getProfile() {
        const url = this.getToken('ss_pic');
        if (url == null || url === ' ') {
            return 'assets/images/NoProfile.png';
        } else {
            return url;
        }
    }

    /****************************************************************************
     //For COOKIE
     /****************************************************************************/
    setCookie(name, value, days) {
        var expires = '';
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = '; expires=' + date.toUTCString();
        }
        document.cookie = name + '=' + (value || '') + expires + '; path=/';
    }

    getCookie(name) {
        var nameEQ = name + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    }

    eraseCookie(name) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }

    screen_size() {
        $('.screen-detail ').height($(window).height() - ($('header ').outerHeight() + $('.screen-tab ').outerHeight() + $('footer ').outerHeight()));
    }

}
